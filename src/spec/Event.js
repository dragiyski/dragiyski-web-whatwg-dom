(function () {
    'use strict';

    const { performance } = require('perf_hooks');
    const {
        validateNativeInvocation,
        validateClassInvocation
    } = require('../errors');
    const symbols = {
        once: Symbol('DOM.Event')
    };

    /**
     * @param {Platform} platform
     */
    module.exports = function (platform) {
        const agent = platform.getImplementation(platform);
        if (symbols.once in agent) {
            return;
        }
        const { DOM } = agent;

        DOM.Event = class Event {
            constructor() {
                this.type = '';
                this.isTrusted = false;
                this.eventPhase = 0;
                this.stopPropagation = false;
                this.stopImmediatePropagation = false;
                this.canceled = false;
                this.inPassiveListener = false;
                this.initialized = false;
                this.dispatch = false;
                this.target = null;
                this.currentTarget = null;
                this.relatedTarget = null;
                this.touchTargetList = [];
                this.path = [];
            }

            /**
             * @see {@link https://dom.spec.whatwg.org/#dom-event-composedpath}
             * @returns {Array<EventTarget>}
             */
            composedPath({ arrayNew, arrayPush, arrayUnshift } = {
                arrayNew: () => [],
                arrayPush: (array, item) => array.push(item),
                arrayUnshift: (array, item) => array.unshift(item)
            }) {
                const composedPath = arrayNew();
                const path = this.path;
                if (path.length === 0) {
                    return composedPath;
                }
                const currentTarget = this.currentTarget;
                arrayPush(composedPath, currentTarget);
                let currentTargetIndex = 0;
                let currentTargetHiddenSubtreeLevel = 0;
                let index = path.length - 1;
                while (index >= 0) {
                    if (path[index].rootOfClosedTree) {
                        ++currentTargetHiddenSubtreeLevel;
                    }
                    if (path[index].invocationTarget === currentTarget) {
                        currentTargetIndex = index;
                        break;
                    }
                    if (path[index].slotInClosedTree) {
                        --currentTargetHiddenSubtreeLevel;
                    }
                    --index;
                }
                let currentHiddenLevel = currentTargetHiddenSubtreeLevel;
                let maxHiddenLevel = currentTargetHiddenSubtreeLevel;
                index = currentTargetIndex - 1;
                while (index >= 0) {
                    if (path[index].rootOfClosedTree) {
                        ++currentHiddenLevel;
                    }
                    if (currentHiddenLevel <= maxHiddenLevel) {
                        arrayUnshift(composedPath, path[index].invocationTarget);
                    }
                    if (path[index].slotInClosedTree) {
                        --currentHiddenLevel;
                        if (currentHiddenLevel < maxHiddenLevel) {
                            maxHiddenLevel = currentHiddenLevel;
                        }
                    }
                    --index;
                }
                currentHiddenLevel = maxHiddenLevel = currentTargetHiddenSubtreeLevel;
                index = currentTargetIndex + 1;
                while (index < path.length) {
                    if (path[index].slotInClosedTree) {
                        ++currentHiddenLevel;
                    }
                    if (currentHiddenLevel < maxHiddenLevel) {
                        arrayPush(composedPath, path[index].invocationTarget);
                    }
                    if (path[index].rootOfClosedTree) {
                        --currentHiddenLevel;
                        if (currentHiddenLevel < maxHiddenLevel) {
                            maxHiddenLevel = currentHiddenLevel;
                        }
                    }
                    ++index;
                }
                return composedPath;
            }

            /**
             * @see {@link https://dom.spec.whatwg.org/#set-the-canceled-flag}
             */
            setCanceled() {
                if (this.cancelable && !this.inPassiveListener) {
                    this.canceled = true;
                }
            }

            /**
             * @see {@link https://dom.spec.whatwg.org/#concept-event-initialize}
             * @param {string} type
             * @param {boolean} bubbles
             * @param {boolean} cancelable
             */
            initialize(type, bubbles, cancelable) {
                this.initialized = true;
                this.stopPropagation = this.stopImmediatePropagation = this.canceled = false;
                this.isTrusted = false;
                this.target = null;
                this.type = type;
                this.bubbles = bubbles;
                this.cancelable = cancelable;
            }

            /**
             * @see {@link https://dom.spec.whatwg.org/#inner-event-creation-steps}
             * @param {function} Implementation
             * @param {number} time
             * @param {object} dictionary
             * @returns {Event}
             */
            static innerEventCreationSteps(Implementation, time = performance.timeOrigin + performance.now(), dictionary = {}) {
                const event = new Implementation();
                event.initialized = true;
                const timeOrigin = agent.timeOrigin || performance.timeOrigin;
                event.timeStamp = time - timeOrigin;
                for (const member in dictionary) {
                    if (Object.hasOwnProperty.call(dictionary, member)) {
                        event[member] = dictionary[member];
                    }
                }
                if (typeof Implementation.eventConstructingSteps === 'function') {
                    Implementation.eventConstructingSteps(event);
                }
                return event;
            }

            /**
             * @see {@link https://dom.spec.whatwg.org/#concept-event-create}
             * @param {function} Implementation
             * @param {number} time
             * @returns {Event}
             */
            static createAnEvent(Implementation, time = performance.timeOrigin + performance.now()) {
                const event = this.innerEventCreationSteps(Implementation, time, {
                    bubbles: false,
                    cancelable: false,
                    composed: false
                });
                event.isTrusted = true;
                const domObject = Object.create(platform.getInterface(Implementation.prototype));
                platform.setImplementation(domObject, event);
                this.unforgeables(domObject);
                return event;
            }

            /**
             * Register the [[LegacyUnforgeable]] properties of an Event interface.
             * @see {@link https://heycam.github.io/webidl/#LegacyUnforgeable}
             * @param event
             */
            static unforgeables(event) {
                Object.defineProperties(event, {
                    isTrusted: {
                        configurable: false,
                        enumerable: true,
                        get: unforgeables.isTrusted.get
                    }
                });
            }
        };

        /**
         * @see {@link https://dom.spec.whatwg.org/#dom-event-eventphase}
         */
        DOM.Event.phase = Object.assign(Object.create(null), {
            NONE: 0,
            CAPTURING_PHASE: 1,
            AT_TARGET: 2,
            BUBBLING_PHASE: 3
        });

        const Event = platform.function(function (type, eventInitDict = Object.create(null)) {
            if (arguments.length < 1) {
                throw new platform.primordials.TypeError(`Failed to construct 'Event': 1 argument required, but only 0 present.`);
            }
            const internalEventInitDict = {
                bubbles: false,
                cancelable: false,
                composed: false
            };
            if (eventInitDict != null && (typeof eventInitDict === 'object' || typeof eventInitDict === 'function')) {
                for (const name in internalEventInitDict) {
                    if (Object.hasOwnProperty.call(internalEventInitDict, name) && Object.hasOwnProperty.call(eventInitDict, name)) {
                        internalEventInitDict[name] = Boolean(eventInitDict[name]);
                    }
                }
            }
            const implObject = DOM.Event.innerEventCreationSteps(DOM.Event, performance.timeOrigin + performance.now(), internalEventInitDict);
            platform.setImplementation(this, implObject);
            implObject.type = '' + type;
            DOM.Event.unforgeables(this);
        }, {
            name: 'Event',
            before: validateClassInvocation
        });

        const method = platform.methodFactory({
            before: [
                validateNativeInvocation,
                platform.unwrapThisInterceptor
            ],
            entry: {
                Interface: Event,
                Implementation: DOM.Event,
                class: 'Event'
            },
            forbidNew: true
        });

        const unforgeables = {
            isTrusted: {
                get: method(function () {
                    return this.isTrusted;
                }, { name: 'isTrusted' })
            }
        };

        Event.prototype = Object.create(platform.primordials['Object.prototype'], {
            constructor: {
                configurable: true,
                writable: true,
                value: Event
            }
        });

        for (const name of [
            'type',
            'eventPhase',
            'bubbles',
            'cancelable',
            'composed',
            'timeStamp'
        ]) {
            Object.defineProperty(Event.prototype, name, {
                configurable: true,
                enumerable: true,
                get: method(function () {
                    return this[name];
                }, { name })
            });
        }

        Object.defineProperties(Event.prototype, {
            srcElement: {
                configurable: true,
                enumerable: true,
                get: method(function () {
                    return this.target;
                }, { name: 'srcElement' })
            },
            target: {
                configurable: true,
                enumerable: true,
                get: method(function () {
                    return this.target;
                }, {
                    name: 'target',
                    after: platform.wrapOwnReturnValueInterceptor
                })
            },
            currentTarget: {
                configurable: true,
                enumerable: true,
                get: method(function () {
                    return this.currentTarget;
                }, {
                    name: 'currentTarget',
                    after: platform.wrapOwnReturnValueInterceptor
                })
            },
            composedPath: {
                configurable: true,
                enumerable: true,
                writable: true,
                value: method(function () {
                    return this.composedPath({
                        arrayNew: () => new platform.primordials.Array(),
                        arrayPush: (array, item) => platform.call(platform.primordials['Array.prototype.push'], array, platform.getInterface(item)),
                        arrayUnshift: (array, item) => platform.call(platform.primordials['Array.prototype.unshift'], array, platform.getInterface(item))
                    });
                }, { name: 'composedPath' })
            },
            stopPropagation: {
                configurable: true,
                enumerable: true,
                writable: true,
                value: method(function () {
                    this.stopPropagation = true;
                }, { name: 'stopPropagation' })
            },
            cancelBubble: {
                configurable: true,
                enumerable: true,
                get: method(function () {
                    return this.stopPropagation;
                }, { name: 'cancelBubble' }),
                set: method(function (value) {
                    if (value === true) {
                        this.stopPropagation = true;
                    }
                }, { name: 'cancelBubble' })
            },
            stopImmediatePropagation: {
                configurable: true,
                enumerable: true,
                writable: true,
                value: method(function () {
                    this.stopPropagation = true;
                    this.stopImmediatePropagation = true;
                }, { name: 'stopImmediatePropagation' })
            },
            returnValue: {
                configurable: true,
                enumerable: true,
                get: method(function () {
                    return !this.canceled;
                }, { name: 'returnValue' }),
                set: method(function (value) {
                    if (value === false) {
                        this.setCanceled();
                    }
                }, { name: 'returnValue' })
            },
            preventDefault: {
                configurable: true,
                enumerable: true,
                writable: true,
                value: method(function () {
                    this.setCanceled();
                }, { name: 'preventDefault' })
            },
            defaultPrevented: {
                configurable: true,
                enumerable: true,
                get: method(function () {
                    return this.canceled;
                }, { name: 'defaultPrevented' })
            },
            initEvent: {
                configurable: true,
                enumerable: true,
                writable: true,
                value: method(function (type, bubbles, cancelable) {
                    if (arguments.length < 1) {
                        throw new platform.primordials.TypeError(`Failed to execute 'initEvent' on 'Event': 1 argument required, but only 0 present.`);
                    }
                    type = '' + type;
                    bubbles = !!bubbles;
                    cancelable = !!cancelable;
                    if (!this.dispatch) {
                        this.initialize(type, bubbles, cancelable);
                    }
                }, { name: 'initEvent' })
            },
            [platform.primordials['Symbol.toStringTag']]: {
                configurable: true,
                value: 'Event'
            }
        });

        for (const phaseName in DOM.Event.phase) {
            for (const target of [Event, Event.prototype]) {
                Object.defineProperty(target, phaseName, {
                    enumerable: true,
                    value: DOM.Event.phase[phaseName]
                });
            }
        }

        platform.setImplementation(Event, DOM.Event);
        platform.setImplementation(Event.prototype, DOM.Event.prototype);

        if (platform.is('Window') || platform.is('Worker') || platform.is('AudioWorklet')) {
            Object.defineProperty(platform.global, 'Event', {
                configurable: true,
                writable: true,
                value: Event
            });
        }

        if (platform.is('Window')) {
            const allowForGlobalOnly = (entry, platform) => {
                if (entry.this !== platform.global) {
                    throw new platform.primordials.TypeError('Illegal invocation');
                }
            };
            Object.defineProperty(platform.global, 'event', {
                configurable: true,
                get: platform.function(function () {
                    return this.currentEvent;
                }, {
                    name: 'event',
                    before: [
                        allowForGlobalOnly,
                        platform.unwrapOwnThisInterceptor
                    ],
                    after: platform.wrapOwnReturnValueInterceptor,
                    forbidNew: true
                }),
                set: platform.function(function (value) {
                    // [Replaceable] means setter redefine the property, so getter is not called anymore.
                    Object.defineProperty(this, 'event', {
                        configurable: true,
                        enumerable: true,
                        writable: true,
                        value
                    });
                }, {
                    name: 'event',
                    before: [
                        allowForGlobalOnly
                    ],
                    forbidNew: true
                })
            });
        }

        agent[symbols.once] = {
            interface: Event,
            implementation: DOM.Event
        };
    };
})();
