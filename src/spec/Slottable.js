(function () {
    'use strict';

    const JavaScriot = require('@dragiyski/javascript');
    const symbols = {
        once: Symbol('DOM.Event')
    };

    /**
     * @param {Platform} platform
     */
    module.exports = function (platform) {
        const agent = platform.getImplementation(platform);
        if (symbols.once in agent) {
            return;
        }
        const { DOM } = agent;

        DOM.Slottable = JavaScriot.mixin(function Slottable() {
            this.assignedSlot = null;
            this.slottableName = '';
        }, {});

        /**
         * @property {boolean} assigned
         * @memberOf DOM.Slottable
         * @instance
         * @readonly
         */
    };
})();
