(function () {
    'use strict';

    const {
        validateNativeInvocation,
        validateClassInvocation
    } = require('../errors');
    const symbols = {
        once: Symbol('DOM.Event')
    };

    /**
     * @param {Platform} platform
     */
    module.exports = function (platform) {
        const agent = platform.getImplementation(platform);
        if (symbols.once in agent) {
            return;
        }
        const { DOM } = agent;

        DOM.AbortController = class AbortController {
            constructor() {
                this.signal = DOM.AbortSignal.create();
            }
        };

        const AbortController = platform.function(function () {
            const implementation = new DOM.AbortController();
            platform.setImplementation(this, implementation);
        }, {
            name: 'AbortController',
            before: validateClassInvocation
        });

        const method = platform.methodFactory({
            before: [
                validateNativeInvocation,
                platform.unwrapThisInterceptor
            ],
            entry: {
                Interface: AbortController,
                Implementation: DOM.AbortController,
                class: 'AbortController'
            },
            forbidNew: true
        });

        AbortController.prototype = Object.create(platform.primordials['Object.prototype'], {
            constructor: {
                configurable: true,
                writable: true,
                value: AbortController
            },
            signal: {
                configurable: true,
                enumerable: true,
                get: method(function () {
                    return this.signal;
                }, {
                    name: 'signal',
                    after: platform.wrapOwnReturnValueInterceptor
                })
            },
            abort: {
                configurable: true,
                enumerable: true,
                writable: true,
                value: method(function abort() {
                    DOM.AbortSignal.signalAbort(this.signal);
                }, { name: 'abort' })
            },
            [platform.primordials['Symbol.toStringTag']]: {
                configurable: true,
                value: 'AbortController'
            }
        });

        platform.setImplementation(AbortController, DOM.AbortController);
        platform.setImplementation(AbortController.prototype, DOM.AbortController.prototype);

        if (platform.is('Window') || platform.is('Worker')) {
            Object.defineProperty(platform.global, 'AbortController', {
                configurable: true,
                writable: true,
                value: AbortController
            });
        }
    };
})();
