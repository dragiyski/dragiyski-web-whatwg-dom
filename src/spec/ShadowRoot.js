(function () {
    'use strict';

    const {
        validateNativeInvocation,
        validateClassInvocation
    } = require('../errors');
    const symbols = {
        once: Symbol('DOM.Event')
    };

    module.exports = function (platform) {
        const agent = platform.getImplementation(platform);
        if (symbols.once in agent) {
            return;
        }
        const { DOM } = agent;

        DOM.ShadowRoot = class ShadowRoot extends DOM.DocumentFragment {
            static retarget(a, b) {
                do {
                    if (!(a instanceof DOM.Node)) {
                        return a;
                    }
                    const root = a.root;
                    if (!(a instanceof DOM.ShadowRoot)) {
                        return a;
                    }
                    if (b instanceof DOM.Node && root.isShadowIncludingInclusiveAncestorOf(b)) {
                        return a;
                    }
                    a = root.host;
                } while (true);
            }
        };
    };
})();
