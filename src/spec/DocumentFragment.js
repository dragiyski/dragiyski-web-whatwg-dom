(function () {
    'use strict';

    const {
        validateNativeInvocation,
        validateClassInvocation
    } = require('../errors');
    const symbols = {
        once: Symbol('DOM.Event')
    };

    module.exports = function (platform) {
        const agent = platform.getImplementation(platform);
        if (symbols.once in agent) {
            return;
        }
        const { DOM } = agent;

        DOM.DocumentFragment = class DocumentFragment extends DOM.Node {
            constructor(document) {
                super();
                this.nodeType = DOM.Node.nodeType.DOCUMENT_FRAGMENT_NODE;
                this.nodeDocument = document;
            }
        };
    };
})();
