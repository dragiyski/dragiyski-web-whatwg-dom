(function () {
    'use strict';

    const {
        validateNativeInvocation
    } = require('../errors');
    const Iterable = require('@dragiyski/collection/iterable');
    const WeakOrderedSet = require('@dragiyski/collection/weak-ordered-set');
    const OrderedMap = require('@dragiyski/collection/ordered-map');
    const OrderedSet = require('@dragiyski/collection/ordered-set');

    const symbols = {
        once: Symbol('DOM.Event'),
        pointer: Symbol('pointer')
    };

    /**
     * @param {Platform} platform
     */
    module.exports = function (platform) {
        const agent = platform.getImplementation(platform);
        if (symbols.once in agent) {
            return;
        }
        const { DOM } = agent;

        /**
         * @class Node
         * @abstract
         */
        DOM.Node = class Node extends DOM.EventTarget {
            constructor(document) {
                super();
                this[symbols.pointer] = Math.floor(Math.random() * Number.MAX_SAFE_INTEGER);
                this.parent = null;
                this.children = new OrderedSet();
                this.nodeDocument = document;
                // Nodes have a strong reference to registered observers in their registered observer list.
                // Registered observers in a node’s registered observer list have a weak reference to the node.
                this.registeredObserverList = new OrderedMap();

                // DOM Specification does not specify how to store live ranges. Based on the algorithms, it seems live range objects live as long as the user
                // holds reference to them. Nodes themselves should not keep live ranges alive, but they need to iterate through all live ranges whose
                // startNode or endNode is strongly referenced to this node.
                // TODO: live range implementation (not the interface) should have setters object when startNode and endNode changes.
                // TODO: The setter must check the old value and remove the reference from the structures below and assign new (weak) reference to the new node.
                this.liveRangeStartNodeSet = new WeakOrderedSet();
                this.liveRangeEndNodeSet = new WeakOrderedSet();
                DOM.NodeList.create(this.children);
            }

            isShadowIncludingInclusiveAncestorOf(node) {
                if (this === node) {
                    return true;
                }
                return this.isShadowIncludingAncestorOf(node);
            }

            isShadowIncludingAncestorOf(node) {
                return node.isShadowIncludingDescendantOf(this);
            }

            isShadowIncludingInclusiveDescendantOf(node) {
                if (this === node) {
                    return true;
                }
                return this.isShadowIncludingDescendantOf(node);
            }

            isShadowIncludingDescendantOf(node) {
                if (this.isDescendantOf(node)) {
                    return true;
                }
                const root = this.root;
                if (root instanceof DOM.ShadowRoot) {
                    return root.host.isShadowIncludingInclusiveDescendantOf(node);
                }
                return false;
            }

            isDescendantOf(node) {
                let parent = this.parent;
                while (parent != null) {
                    if (parent === node) {
                        return true;
                    }
                    parent = parent.parent;
                }
                return false;
            }

            isInclusiveDescendantOf(node) {
                if (this === node) {
                    return true;
                }
                return this.isDescendantOf(node);
            }

            isAncestorOf(node) {
                return node.isDescendantOf(this);
            }

            isInclusiveAncestorOf(node) {
                if (this === node) {
                    return true;
                }
                return this.isAncestorOf(node);
            }

            isSiblingOf(node) {
                return this.parent != null && node.parent != null && this.parent === node.parent;
            }

            isInclusiveSiblingOf(node) {
                if (this === node) {
                    return true;
                }
                return this.isSiblingOf(node);
            }

            isPreceding(node) {
                // null is never preceding
                // Node cannot precede itself
                if (node == null || node === this) {
                    return false;
                }
                // If this is a root node, it precedes all nodes whose root if this node.
                if (this.parent == null) {
                    return node.root === this;
                }
                // Otherwise, if the other node is root, it never precedes:
                // 1. Either nodes are disjointed (they are both roots)
                // 2. Or this node is descendant of this node. In that case it does not precede.
                if (node.parent == null) {
                    return false;
                }
                // Otherwise loop through all pairs of ancestors...
                for (let node1 = this; node1.parent != null; node1 = node1.parent) {
                    for (let node2 = node; node2.parent != null; node2 = node2.parent) {
                        // If a common ancestor is found, return based on the node index.
                        if (node1.parent === node2.parent) {
                            return node1.index < node2.index;
                        }
                    }
                }
                // If common ancestor is not found, the nodes are disjointed.
                return false;
            }

            isFollowing(node) {
                // null is never following
                // Node cannot follow itself
                if (node == null || node === this) {
                    return false;
                }
                // If this is a root node, it never follows:
                // 1. Either nodes are disjointed (they are both roots)
                // 2. Or the other node is descendant of this node. In that case it does not follow.
                if (this.parent == null) {
                    return false;
                }
                // Otherwise, if the other node is a root node, this node follows if the other node is this node's root.
                if (node.parent == null) {
                    return this.root === node;
                }
                // Otherwise loop through all pairs of ancestors...
                for (let node1 = this; node1.parent != null; node1 = node1.parent) {
                    for (let node2 = node; node2.parent != null; node2 = node2.parent) {
                        // If a common ancestor is found, return based on the node index.
                        if (node1.parent === node2.parent) {
                            return node1.index > node2.index;
                        }
                    }
                }
                // If common ancestor is not found, the nodes are disjointed.
                return false;
            }

            get root() {
                let object = this;
                while (object.parent != null) {
                    object = object.parent;
                }
                return object;
            }

            get shadowIncludingRoot() {
                const root = this.root;
                if (root instanceof DOM.ShadowRoot) {
                    return root.host.shadowIncludingRoot;
                }
                return root;
            }

            get firstChild() {
                if (this.children.length > 0) {
                    return this.children[0];
                }
                return null;
            }

            get lastChild() {
                if (this.children.length > 0) {
                    return this.children[this.children.length - 1];
                }
                return null;
            }

            get index() {
                if (this.parent == null) {
                    return 0;
                }
                return this.parent.children.indexOf(this);
            }

            get previousSibling() {
                if (this.parent == null) {
                    return null;
                }
                const index = this.index;
                if (index > 0) {
                    return this.parent.children[index - 1];
                }
                return null;
            }

            get nextSibling() {
                if (this.parent == null) {
                    return null;
                }
                const index = this.index;
                if (index < this.parent.children.length - 1) {
                    return this.parent.children[index + 1];
                }
                return null;
            }

            /**
             * @return {DOM.Node}
             */
            getTheParent() {
                return this.assigned ? this.assignedSlot : this.parent;
            }

            get isAssigned() {
                return this instanceof DOM.Slottable && this.assignedSlot != null;
            }

            get isConnected() {
                const root = this.shadowIncludingRoot;
                return root != null && root.nodeType === DOM.Node.nodeType.DOCUMENT_NODE;
            }

            /**
             * An optimized property for the interface. DOM.Document can override that to return null (while nodeDocument === this).
             * @return {DOM.Document}
             */
            get ownerDocument() {
                return this.nodeDocument;
            }

            get parentElement() {
                const parent = this.parent;
                if (parent != null && DOM.Element.is(parent)) {
                    return parent;
                }
                return null;
            }

            get nodeValue() {
                return null;
            }

            set nodeValue(value) {
            }

            get textContent() {
                return null;
            }

            set textContent(value) {
            }

            get length() {
                return this.children.length;
            }

            static isSlot(node) {
                return DOM.Element.is(node) && node.localName === 'slot' && node.namespace == null;
            }

            normalize() {
                // The normalize() method, when invoked, must run these steps for each descendant exclusive Text node node of this:
                for (const node of Iterable(this.topDownDescendantIterator()).filter(node => DOM.Text.isExclusiveTextNode(node))) {
                    // 1. Let length be node’s length.
                    let length = node.length;
                    // 2. If length is zero, then remove node and continue with the next exclusive Text node, if any.
                    if (length === 0) {
                        DOM.Node.remove(node);
                        continue;
                    }
                    // 3. Let data be the concatenation of the data of node’s contiguous exclusive Text nodes (excluding itself), in tree order.
                    const data = Iterable(this.contiguosExclusiveTextNodes()).map(node => node.data).join('');
                    // 4. Replace data with node node, offset length, count 0, and data data.
                    DOM.CharacterData.replaceData(node, length, 0, data);
                    // 5. Let currentNode be node’s next sibling.
                    let currentNode = node.nextSibling;
                    // 6. While currentNode is an exclusive Text node:
                    while (DOM.Text.isExclusiveTextNode(currentNode)) {
                        // 6.1. For each live range whose start node is currentNode, add length to its start offset and set its start node to node.
                        for (const liveRange of currentNode.liveRangeStartNodeSet) {
                            liveRange.startOffset += length;
                            liveRange.startNode = node;
                        }
                        // 6.2. For each live range whose end node is currentNode, add length to its end offset and set its end node to node.
                        for (const liveRange of currentNode.liveRangeEndNodeSet) {
                            liveRange.endOffset += length;
                            liveRange.endNode = node;
                        }
                        // 6.3. For each live range whose start node is currentNode’s parent and start offset is currentNode’s index, set its start node to node and its start offset to length.
                        for (const liveRange of currentNode.parent.liveRangeStartNodeSet) {
                            if (liveRange.startOffset === currentNode.index) {
                                liveRange.startNode = node;
                                liveRange.startOffset = length;
                            }
                        }
                        // 6.4. For each live range whose end node is currentNode’s parent and end offset is currentNode’s index, set its end node to node and its end offset to length.
                        for (const liveRange of currentNode.parent.liveRangeEndNodeSet) {
                            if (liveRange.endOffset === currentNode.index) {
                                liveRange.endNode = node;
                                liveRange.endOffset = length;
                            }
                        }
                        // 6.5. Add currentNode’s length to length.
                        length += currentNode.length;
                        // 6.6. Set currentNode to its next sibling.
                        currentNode = currentNode.nextSibling;
                    }
                    // 7. Remove node’s contiguous exclusive Text nodes (excluding itself), in tree order.
                    for (const removeNode of this.contiguosExclusiveTextNodes()) {
                        if (node === removeNode) {
                            continue;
                        }
                        DOM.Node.remove(node);
                    }
                }
            }

            clone(document, cloneChildrenFlag = false) {
                // 1. If document is not given, let document be node’s node document.
                if (document == null) {
                    document = this.nodeDocument;
                }
                // 2. If node is ...
                // 3. Otherwise, let copy be a node that implements the same interfaces as node, and fulfills these additional requirements, switching on node:
                // TODO: Override this method in each relevant subclass. The default just create private/public interface from the same class as "this"
                const copy = this.cloneImpl(document);
                // 4. Set copy’s node document and document to copy, if copy is a document, and set copy’s node document to document otherwise.
                copy.nodeDocument = DOM.Document.is(copy) ? copy : document;
                // 5. Run any cloning steps defined for node in other applicable specifications and pass copy, node, document and the clone children flag if set, as parameters.
                if (typeof this.cloningSteps === 'function') {
                    this.cloningSteps(copy, document, cloneChildrenFlag);
                }
                // 6. If the clone children flag is set, clone all the children of node and append them to copy, with document as specified and the clone children flag being set.
                if (cloneChildrenFlag) {
                    return Iterable(this.children)
                        .map(child => child.clone(document, true))
                        .forEach(child => copy.children.append(child));
                }
                return copy;
            }

            cloneImpl(document) {
                const implementation = new this.constructor(document);
                const domObject = Object.create(platform.getInterface(this.constructor.prototype));
                platform.setImplementation(domObject, implementation);
                return implementation;
            }

            cloneNode(deep) {
                // TODO: 1. If this is a shadow root, then throw a "NotSupportedError" DOMException.
                // TODO: to be overridden in DOM.ShadowRoot
                return this.clone(null, Boolean(deep));
            }

            equals(node) {
                return this.nodeType === node.nodeType;
                // TODO: To be overridden by each specific subclass
            }

            isEqualNode(otherNode) {
                return otherNode != null && this.equals(otherNode);
            }

            get implementationId() {
                return this[symbols.pointer];
            }

            compareDocumentPosition(other) {
                // 1. If this is other, then return zero.
                if (this === other) {
                    return 0;
                }
                // 2. Let node1 be other and node2 be this.
                let node1 = other;
                let node2 = this;
                // 3. Let attr1 and attr2 be null.
                let attr1 = null;
                let attr2 = null;
                // 4. If node1 is an attribute, then set attr1 to node1 and node1 to attr1’s element.
                if (DOM.Attr.is(node1)) {
                    attr1 = node1;
                    node1 = attr1.element;
                }
                // 5. If node2 is an attribute, then:
                if (DOM.Attr.is(node2)) {
                    // 5.1. Set attr2 to node2 and node2 to attr2’s element.
                    attr2 = node2;
                    node2 = attr2.element;
                    if (attr1 != null && node1 != null && node2 === node1) {
                        for (const attr of node2.attributeList.values()) {
                            if (attr === attr1) {
                                return DOM.Node.documentPosition.DOCUMENT_POSITION_IMPLEMENTATION_SPECIFIC |
                                    DOM.Node.documentPosition.DOCUMENT_POSITION_PRECEDING;
                            }
                            if (attr === attr2) {
                                return DOM.Node.documentPosition.DOCUMENT_POSITION_IMPLEMENTATION_SPECIFIC |
                                    DOM.Node.documentPosition.DOCUMENT_POSITION_FOLLOWING;
                            }
                        }
                    }
                }
                // 6. If node1 or node2 is null, or node1’s root is not node2’s root, then return the result of adding DOCUMENT_POSITION_DISCONNECTED, DOCUMENT_POSITION_IMPLEMENTATION_SPECIFIC, and either DOCUMENT_POSITION_PRECEDING or DOCUMENT_POSITION_FOLLOWING, with the constraint that this is to be consistent, together.
                // Note: Whether to return DOCUMENT_POSITION_PRECEDING or DOCUMENT_POSITION_FOLLOWING is typically implemented via pointer comparison. In JavaScript implementations a cached Math.random() value can be used.
                if (node1 == null || node2 == null || node1.root !== node2.root) {
                    const ptrNode1 = node1?.implementationId ?? 0;
                    const ptrNode2 = node2?.implementationId ?? 0;
                    return DOM.Node.documentPosition.DOCUMENT_POSITION_DISCONNECTED |
                        DOM.Node.documentPosition.DOCUMENT_POSITION_IMPLEMENTATION_SPECIFIC |
                        (ptrNode1 < ptrNode2 ? DOM.Node.documentPosition.DOCUMENT_POSITION_PRECEDING : DOM.Node.documentPosition.DOCUMENT_POSITION_FOLLOWING);
                }
                if (node1.isAncestorOf(node2) && attr1 == null || node1 === node2 && attr2 != null) {
                    return DOM.Node.documentPosition.DOCUMENT_POSITION_CONTAINS |
                        DOM.Node.documentPosition.DOCUMENT_POSITION_PRECEDING;
                }
                if (node1.isDescendantOf(node2) && attr2 == null || node1 === node2 && attr1 != null) {
                    return DOM.Node.documentPosition.DOCUMENT_POSITION_CONTAINED_BY |
                        DOM.Node.documentPosition.DOCUMENT_POSITION_FOLLOWING;
                }
                if (node1.isPreceding(node2)) {
                    return DOM.Node.documentPosition.DOCUMENT_POSITION_PRECEDING;
                }
                return DOM.Node.documentPosition.DOCUMENT_POSITION_FOLLOWING;
            }

            contains(other) {
                return other != null && other.isInclusiveDescendantOf(this);
            }

            locateNamespacePrefix() {

            }
        };

        /**
         * Get all node descendants in a tree order (pre-order depth-first order).
         * @return {Generator<DOM.Node>}
         */
        DOM.Node.prototype.topDownDescendantIterator = function * () {
            for (let i = 0; i < this.children.length; ++i) {
                yield this.children[i];
                yield * this.children[i].topDownDescendantIterator();
            }
        };

        DOM.Node.prototype.bottomUpDescendantIterator = function * () {
            for (let i = 0; i < this.children.length; ++i) {
                yield * this.children[i].bottomUpDescendantIterator();
                yield this.children[i];
            }
        };

        Object.defineProperties(DOM.Node, {
            nodeType: {
                configurable: true,
                writable: true,
                value: {
                    ELEMENT_NODE: 1,
                    ATTRIBUTE_NODE: 2,
                    TEXT_NODE: 3,
                    CDATA_SECTION_NODE: 4,
                    ENTITY_REFERENCE_NODE: 5,
                    ENTITY_NODE: 6,
                    PROCESSING_INSTRUCTION_NODE: 7,
                    COMMENT_NODE: 8,
                    DOCUMENT_NODE: 9,
                    DOCUMENT_TYPE_NODE: 10,
                    DOCUMENT_FRAGMENT_NODE: 11
                }
            },
            documentPosition: {
                configurable: true,
                writable: true,
                value: {
                    DOCUMENT_POSITION_DISCONNECTED: 1,
                    DOCUMENT_POSITION_PRECEDING: 2,
                    DOCUMENT_POSITION_FOLLOWING: 4,
                    DOCUMENT_POSITION_CONTAINS: 8,
                    DOCUMENT_POSITION_CONTAINED_BY: 16,
                    DOCUMENT_POSITION_IMPLEMENTATION_SPECIFIC: 32
                }
            }
        });

        const Node = platform.function(function () {
            throw new platform.primordials.TypeError('Illegal constructor');
        }, {
            name: 'Node'
        });

        const noReturn = platform.methodFactory({
            before: [
                validateNativeInvocation,
                platform.unwrapThisInterceptor
            ],
            entry: {
                Interface: Node,
                Implementation: DOM.Node,
                class: 'Node'
            },
            forbidNew: true
        });

        const withReturn = platform.methodFactory({
            before: [
                validateNativeInvocation,
                platform.unwrapThisInterceptor
            ],
            after: platform.wrapReturnValueInterceptor,
            entry: {
                Interface: Node,
                Implementation: DOM.Node,
                class: 'Node'
            },
            forbidNew: true
        });

        Node.prototype = Object.create(platform.getInterface(DOM.EventTarget.prototype), {
            constructor: {
                configurable: true,
                writable: true,
                value: Node
            },
            nodeType: {
                configurable: true,
                enumerable: true,
                get: noReturn(function () {
                    return this.nodeType;
                }, { name: 'nodeType' })
            },
            nodeName: {
                configurable: true,
                enumerable: true,
                get: noReturn(function () {
                    return this.nodeName;
                }, { name: 'nodeName' })
            },
            baseURI: {
                configurable: true,
                enumerable: true,
                get: noReturn(function () {
                    return '' + this.nodeDocument.baseURL;
                }, { name: 'nodeName' })
            },
            isConnected: {
                configurable: true,
                enumerable: true,
                get: noReturn(function () {
                    return Boolean(this.isConnected);
                }, { name: 'isConnected' })
            },
            ownerDocument: {
                configurable: true,
                enumerable: true,
                get: noReturn(function () {
                    return Boolean(this.ownerDocument);
                }, { name: 'ownerDocument' })
            },
            getRootNode: {
                configurable: true,
                enumerable: true,
                writable: true,
                value: withReturn(function (options = Object.create(null)) {
                    return options.composed ? this.shadowIncludingRoot : this.root;
                }, { name: 'getRootNode' })
            },
            parentNode: {
                configurable: true,
                enumerable: true,
                get: withReturn(function () {
                    return this.parent;
                }, { name: 'parentNode' })
            },
            parentElement: {
                configurable: true,
                enumerable: true,
                get: withReturn(function () {
                    return this.parentElement;
                }, { name: 'parentElement' })
            },
            hasChildNodes: {
                configurable: true,
                enumerable: true,
                writable: true,
                value: noReturn(function () {
                    return this.children.length > 0;
                }, { name: 'hasChildNodes' })
            },
            childNodes: {
                configurable: true,
                enumerable: true,
                get: withReturn(function () {
                    return this.children;
                }, { name: 'childNodes' })
            },
            firstChild: {
                configurable: true,
                enumerable: true,
                get: withReturn(function () {
                    return this.firstChild;
                }, { name: 'firstChild' })
            },
            lastChild: {
                configurable: true,
                enumerable: true,
                get: withReturn(function () {
                    return this.lastChild;
                }, { name: 'lastChild' })
            },
            previousSibling: {
                configurable: true,
                enumerable: true,
                get: withReturn(function () {
                    return this.previousSibling;
                }, { name: 'previousSibling' })
            },
            nextSibling: {
                configurable: true,
                enumerable: true,
                get: withReturn(function () {
                    return this.nextSibling;
                }, { name: 'nextSibling' })
            },
            nodeValue: {
                configurable: true,
                enumerable: true,
                get: noReturn(function () {
                    return this.nodeValue;
                }, { name: 'nodeValue' }),
                set: noReturn(function (value) {
                    this.nodeValue = value;
                    return true;
                }, { name: 'nodeValue' })
            },
            textContent: {
                configurable: true,
                enumerable: true,
                get: noReturn(function () {
                    return this.textContent;
                }, { name: 'textContent' }),
                set: noReturn(function (value) {
                    this.textContent = value;
                    return true;
                }, { name: 'textContent' })
            },
            cloneNode: {
                configurable: true,
                enumerable: true,
                writable: true,
                value: withReturn(function (deep) {
                    return this.cloneNode(deep);
                }, { name: 'cloneNode' })
            },
            isEqualNode: {
                configurable: true,
                enumerable: true,
                writable: true,
                value: noReturn(function (otherNode) {
                    return this.isEqualNode(otherNode);
                }, {
                    name: 'isEqualNode',
                    before: [
                        (entry, platform) => {
                            if (entry.arguments.length < 1) {
                                throw new platform.primordials.TypeError(`Failed to execute 'isEqualNode' on 'Node': 1 argument required, but only 0 present.`);
                            }
                        },
                        platform.unwrapArgumentsInterceptorFactory(0),
                        (entry, platform) => {
                            if (!(entry.arguments[0] instanceof DOM.Node)) {
                                throw new platform.primordials.TypeError(`Failed to execute 'isEqualNode' on 'Node': parameter 1 is not of type 'Node'.`);
                            }
                        }
                    ]
                })
            },
            isSameNode: {
                configurable: true,
                enumerable: true,
                writable: true,
                value: noReturn(function (otherNode) {
                    return this === otherNode;
                }, {
                    name: 'isSameNode',
                    before: [
                        (entry, platform) => {
                            if (entry.arguments.length < 1) {
                                throw new platform.primordials.TypeError(`Failed to execute 'isSameNode' on 'Node': 1 argument required, but only 0 present.`);
                            }
                        },
                        platform.unwrapArgumentsInterceptorFactory(0),
                        (entry, platform) => {
                            if (!(entry.arguments[0] instanceof DOM.Node)) {
                                throw new platform.primordials.TypeError(`Failed to execute 'isSameNode' on 'Node': parameter 1 is not of type 'Node'.`);
                            }
                        }
                    ]
                })
            },
            compareDocumentPosition: {
                configurable: true,
                enumerable: true,
                writable: true,
                value: noReturn(function (other) {
                    return this.compareDocumentPosition(other);
                }, {
                    name: 'isSameNode',
                    before: [
                        (entry, platform) => {
                            if (entry.arguments.length < 1) {
                                throw new platform.primordials.TypeError(`Failed to execute 'compareDocumentPosition' on 'Node': 1 argument required, but only 0 present.`);
                            }
                        },
                        platform.unwrapArgumentsInterceptorFactory(0),
                        (entry, platform) => {
                            if (!(entry.arguments[0] instanceof DOM.Node)) {
                                throw new platform.primordials.TypeError(`Failed to execute 'compareDocumentPosition' on 'Node': parameter 1 is not of type 'Node'.`);
                            }
                        }
                    ]
                })
            },
            contains: {
                configurable: true,
                enumerable: true,
                writable: true,
                value: noReturn(function (other) {
                    return this.contains(other);
                }, {
                    name: 'isSameNode',
                    before: [
                        (entry, platform) => {
                            if (entry.arguments.length < 1) {
                                throw new platform.primordials.TypeError(`Failed to execute 'compareDocumentPosition' on 'Node': 1 argument required, but only 0 present.`);
                            }
                        },
                        platform.unwrapArgumentsInterceptorFactory(0),
                        (entry, platform) => {
                            if (!(entry.arguments[0] instanceof DOM.Node)) {
                                throw new platform.primordials.TypeError(`Failed to execute 'compareDocumentPosition' on 'Node': parameter 1 is not of type 'Node'.`);
                            }
                        }
                    ]
                })
            }
        });
    };
})();
