(function () {
    'use strict';

    const symbols = {
        once: Symbol('DOM.Event')
    };

    /**
     * @param {Platform} platform
     */
    module.exports = function (platform) {
        const agent = platform.getImplementation(platform);
        if (symbols.once in agent) {
            return;
        }
        const { DOM } = agent;

        /**
         * NodeList wraps up and array, not a class instance...
         */
        DOM.NodeList = {
            proxyMap: new WeakMap(),
            create: function (array) {
                const domObject = Object.create(this['NodeList.prototype']);
                const domProxy = new platform.primordials.Proxy(domObject, this.indexHandler);
                DOM.NodeList.proxyMap.set(domObject, domProxy);
                platform.setImplementation(domProxy, array);
                return domProxy;
            },
            indexHandler: {
                has: (target, property) => {
                    if (typeof property === 'string') {
                        const index = parseInt(property, 10);
                        const proxyMap = DOM.NodeList.proxyMap;
                        if (isFinite(index) && index >= 0 && index.toString(10) === property && proxyMap.has(target)) {
                            const proxy = proxyMap.get(target);
                            if (platform.hasImplementation(proxy)) {
                                const implementation = platform.getImplementation(proxy);
                                return index < implementation.length;
                            }
                        }
                    }
                    return Reflect.has(target, property);
                },
                get: (target, property, receiver) => {
                    if (typeof property === 'string') {
                        const index = parseInt(property, 10);
                        const proxyMap = DOM.NodeList.proxyMap;
                        if (isFinite(index) && index >= 0 && index.toString(10) === property && proxyMap.has(target)) {
                            const proxy = proxyMap.get(target);
                            if (platform.hasImplementation(proxy)) {
                                const implementation = platform.getImplementation(proxy);
                                if (index < implementation.length) {
                                    return platform.getInterface(implementation[index]);
                                }
                            }
                        }
                    }
                    return Reflect.get(target, property, receiver);
                },
                set: (target, property, value, receiver) => {
                    if (typeof property === 'string') {
                        const index = parseInt(property, 10);
                        if (isFinite(index) && index.toString(10) === property && index >= 0) {
                            throw new platform.primordials.TypeError(`Failed to set an indexed property on 'NodeList': Indexed property setter is not supported.`);
                        }
                    }
                    return Reflect.set(target, property, value, receiver);
                }
            },
            createIterator: function (array, item) {
                return DOM.createArrayIterator(Object.create(Object.prototype, {
                    length: {
                        configurable: true,
                        enumerable: true,
                        get: function () {
                            return array.length;
                        }
                    },
                    index: {
                        configurable: true,
                        enumerable: true,
                        writable: true,
                        value: 0
                    },
                    item: {
                        configurable: true,
                        enumerable: true,
                        writable: true,
                        value: item
                    }
                }));
            }
        };

        const NodeList = platform.function(function () {
            throw new TypeError('Illegal constructor');
        }, {
            name: 'NodeList'
        });

        function isValidImplementation(implementation) {
            if (implementation == null || typeof implementation !== 'object') {
                return false;
            }
            return typeof implementation[Symbol.iterator] === 'function' && typeof implementation.length === 'number';
        }

        const method = platform.methodFactory({
            before: [
                (entry, platform) => {
                    if (!(entry.this instanceof entry.Interface) || !isValidImplementation(platform.getImplementation(entry.this))) {
                        throw new platform.primordials.TypeError('Illegal invocation');
                    }
                },
                platform.unwrapThisInterceptor
            ],
            entry: {
                Interface: NodeList,
                class: 'NodeList'
            },
            forbidNew: true
        });

        DOM.NodeList['NodeList.prototype'] = NodeList.prototype = Object.create(platform.primordials['Object.prototype'], {
            constructor: {
                configurable: true,
                writable: true,
                value: NodeList
            },
            entries: {
                configurable: true,
                enumerable: true,
                writable: true,
                value: method(function () {
                    return DOM.ArrayIterator.create(this, (element, index) => {
                        const item = new platform.primordials.Array(2);
                        item[0] = index;
                        item[1] = platform.getInterface(element);
                        return item;
                    });
                }, {
                    name: 'values',
                    after: platform.wrapReturnValueInterceptor
                })
            },
            keys: {
                configurable: true,
                enumerable: true,
                writable: true,
                value: method(function () {
                    return DOM.ArrayIterator.create(this, (element, index) => {
                        return index;
                    });
                }, {
                    name: 'keys',
                    after: platform.wrapReturnValueInterceptor
                })
            },
            values: {
                configurable: true,
                enumerable: true,
                writable: true,
                value: method(function () {
                    return DOM.ArrayIterator.create(this, (element, index) => {
                        return platform.getInterface(element);
                    });
                }, {
                    name: 'values',
                    after: platform.wrapReturnValueInterceptor
                })
            },
            forEach: {
                configurable: true,
                enumerable: true,
                writable: true,
                value: method(function (callback, thisArg) {
                    if (typeof callback !== 'function') {
                        throw new platform.primordials.TypeError(`${Object.prototype.toString.call(callback)} is not a function`);
                    }
                    if (arguments.length < 2) {
                        thisArg = platform.global;
                    }
                    const domObject = platform.getOwnInterface(this);
                    for (let i = 0; i < this.length; ++i) {
                        platform.call(callback, thisArg, platform.getInterface(this[i]), i, domObject);
                    }
                }, { name: 'forEach' })
            },
            item: {
                configurable: true,
                enumerable: true,
                writable: true,
                value: method(function (index) {
                    if (arguments.length < 1) {
                        throw new platform.primordials.TypeError(`Failed to execute 'item' on 'NodeList': 1 argument required, but only 0 present.`);
                    }
                    index = parseInt(index, 10);
                    if (index >= 0 && index < this.length) {
                        return this[index];
                    }
                    return null;
                }, {
                    name: 'item',
                    after: platform.wrapReturnValueInterceptor
                })
            },
            length: {
                configurable: true,
                enumerable: true,
                get: method(function () {
                    return this.length;
                }, { name: 'length' })
            },
            [platform.primordials['Symbol.toStringTag']]: {
                configurable: true,
                value: 'NodeList'
            }
        });

        Object.defineProperty(NodeList.prototype, platform.primordials['Symbol.iterator'], {
            configurable: true,
            writable: true,
            value: NodeList.prototype.values
        });
    };
})();
