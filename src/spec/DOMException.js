(function () {
    'use strict';

    const {
        validateNativeInvocation,
        validateClassInvocation
    } = require('../errors');

    const symbols = {
        once: Symbol('DOM.Event')
    };

    /**
     * @param {Platform} platform
     */
    module.exports = function (platform) {
        const agent = platform.getImplementation(platform);
        if (symbols.once in agent) {
            return;
        }
        const { DOM } = agent;

        DOM.DOMException = class DOMException extends Error {
            constructor(message = '', name = 'Error', code = true) {
                super();
                this.name = name;
                this.message = message;
                if (typeof code === 'number') {
                    this.code = code;
                } else if (code === true && Object.hasOwnProperty.call(this.nameToCode, name) && typeof this.nameToCode[name] === 'number') {
                    this.code = this.nameToCode[name];
                } else {
                    this.code = 0;
                }
            }

            static create(message = '', name = 'Error', code = true) {
                const domObject = Object.create(platform.getInterface(this.prototype));
                return this.initialize(domObject, message, name, code);
            }

            static initialize(domObject, message = '', name = 'Error', code = 0) {
                const implementation = new DOMException(message, name, code);
                Error.captureStackTrace(implementation);
                platform.setImplementation(domObject, implementation);
                return implementation;
            }
        };

        Object.defineProperties(DOM.DOMException.prototype, {
            nameToCode: {
                configurable: true,
                writable: true,
                value: {
                    INDEX_SIZE_ERR: 1,
                    DOMSTRING_SIZE_ERR: 2,
                    HIERARCHY_REQUEST_ERR: 3,
                    WRONG_DOCUMENT_ERR: 4,
                    INVALID_CHARACTER_ERR: 5,
                    NO_DATA_ALLOWED_ERR: 6,
                    NO_MODIFICATION_ALLOWED_ERR: 7,
                    NOT_FOUND_ERR: 8,
                    NOT_SUPPORTED_ERR: 9,
                    INUSE_ATTRIBUTE_ERR: 10,
                    INVALID_STATE_ERR: 11,
                    SYNTAX_ERR: 12,
                    INVALID_MODIFICATION_ERR: 13,
                    NAMESPACE_ERR: 14,
                    INVALID_ACCESS_ERR: 15,
                    VALIDATION_ERR: 16,
                    TYPE_MISMATCH_ERR: 17,
                    SECURITY_ERR: 18,
                    NETWORK_ERR: 19,
                    ABORT_ERR: 20,
                    URL_MISMATCH_ERR: 21,
                    QUOTA_EXCEEDED_ERR: 22,
                    TIMEOUT_ERR: 23,
                    INVALID_NODE_TYPE_ERR: 24,
                    DATA_CLONE_ERR: 25
                }
            }
        });

        const DOMException = platform.function(function (message = '', name = 'Error') {
            message = '' + message;
            name = '' + name;
            DOM.DOMException.initialize(this, message, name);
        }, {
            name: 'DOMException',
            before: validateClassInvocation
        });

        const method = platform.methodFactory({
            before: [
                validateNativeInvocation,
                platform.unwrapThisInterceptor
            ],
            entry: {
                Interface: DOMException,
                Implementation: DOM.DOMException,
                class: 'DOMException'
            },
            forbidNew: true
        });

        DOMException.prototype = Object.create(platform.primordials['Error.prototype'], {
            constructor: {
                configurable: true,
                writable: true,
                value: DOMException
            },
            message: {
                configurable: true,
                enumerable: true,
                get: method(function () {
                    return this.message;
                }, { name: 'message' })
            },
            name: {
                configurable: true,
                enumerable: true,
                get: method(function () {
                    return this.name;
                }, { name: 'name' })
            },
            code: {
                configurable: true,
                enumerable: true,
                get: method(function () {
                    return this.code;
                }, { name: 'code' })
            },
            [platform.primordials['Symbol.toStringTag']]: {
                configurable: true,
                enumerable: true,
                writable: true,
                value: 'DOMException'
            }
        });

        for (const errName in DOM.DOMException.prototype.nameToCode) {
            if (Object.hasOwnProperty.call(DOM.DOMException.prototype.nameToCode, errName)) {
                for (const target of [DOMException, DOMException.prototype]) {
                    Object.defineProperty(target, errName, {
                        enumerable: true,
                        value: DOM.DOMException.prototype.nameToCode[errName]
                    });
                }
            }
        }

        platform.setImplementation(DOMException, DOM.DOMException);
        platform.setImplementation(DOMException.prototype, DOM.DOMException.prototype);

        if (platform.is('Window') || platform.is('Worker')) {
            platform.primordials['Object.defineProperty'](platform.global, 'DOMException', {
                configurable: true,
                writable: true,
                value: DOMException
            });
        }

        agent[symbols.once] = {
            interface: DOMException,
            implementation: DOM.DOMException
        };
    };
})();
