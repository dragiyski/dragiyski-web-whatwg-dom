(function () {
    'use strict';

    const OrderedSet = require('@dragiyski/collection/ordered-set');

    const {
        validateNativeInvocation
    } = require('../errors');
    const symbols = {
        once: Symbol('DOM.Event')
    };

    /**
     * @param {Platform} platform
     */
    module.exports = function (platform) {
        const agent = platform.getImplementation(platform);
        if (symbols.once in agent) {
            return;
        }
        const { DOM } = agent;

        DOM.AbortSignal = class AbortSignal extends DOM.EventTarget {
            constructor() {
                super();
                this.aborted = false;
                this.abortAlgorithms = new OrderedSet();
            }

            static create() {
                const implementation = new this();
                const domObject = Object.create(platform.getInterface(this.prototype));
                platform.setImplementation(domObject, implementation);
                return implementation;
            }

            static addAlgorithm(algorithm, signal) {
                if (typeof algorithm !== 'function') {
                    throw new TypeError('Expected algorithm to be callable');
                }
                if (signal.aborted) {
                    return;
                }
                signal.abortAlgorithms.append(algorithm);
            }

            static removeAlgorithm(algorithm, signal) {
                signal.abortAlgorithms.remove(algorithm);
            }

            static signalAbort(signal) {
                if (signal.aborted) {
                    return;
                }
                signal.aborted = true;
                for (const algorithm of signal.abortAlgorithms) {
                    algorithm(signal);
                }
                signal.abortAlgorithms.clear();
                DOM.EventTarget.fireAnEvent('abort', signal);
            }
        };

        const AbortSignal = platform.function(function () {
            throw platform.primordials.TypeError('Illegal constructor');
        }, {
            name: 'AbortSignal'
        });

        const method = platform.methodFactory({
            before: [
                validateNativeInvocation,
                platform.unwrapThisInterceptor
            ],
            entry: {
                Interface: AbortSignal,
                Implementation: DOM.AbortSignal,
                class: 'AbortSignal'
            },
            forbidNew: true
        });

        AbortSignal.prototype = Object.create(platform.getOwnInterface(DOM.EventTarget.prototype), {
            constructor: {
                configurable: true,
                writable: true,
                value: AbortSignal
            },
            aborted: {
                configurable: true,
                enumerable: true,
                get: method(function () {
                    return this.aborted;
                }, { name: 'aborted' })
            },
            [platform.primordials['Symbol.toStringTag']]: {
                configurable: true,
                value: 'AbortSignal'
            }
        });

        platform.setImplementation(AbortSignal, DOM.AbortSignal);
        platform.setImplementation(AbortSignal.prototype, DOM.AbortSignal.prototype);

        if (platform.is('Window') || platform.is('Worker')) {
            Object.defineProperty(platform.global, 'AbortSignal', {
                configurable: true,
                writable: true,
                value: AbortSignal
            });
        }
    };
})();
