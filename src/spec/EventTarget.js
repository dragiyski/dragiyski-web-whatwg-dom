(function () {
    'use strict';

    const {
        validateNativeInvocation,
        validateClassInvocation
    } = require('../errors');
    const symbols = {
        once: Symbol('DOM.Event')
    };

    module.exports = function (platform) {
        const agent = platform.getImplementation(platform);
        if (symbols.once in agent) {
            return;
        }
        const { DOM } = agent;

        DOM.EventTarget = class EventTarget {
            constructor() {
                /**
                 * @see {@link https://dom.spec.whatwg.org/#eventtarget-event-listener-list}
                 */
                this.eventListenerList = [];
            }

            /**
             * @see {@link https://dom.spec.whatwg.org/#get-the-parent}
             * @returns {null}
             */
            getTheParent() {
                return null;
            }

            /**
             * @see {@link https://dom.spec.whatwg.org/#concept-flatten-options}
             * @param options
             * @returns {boolean}
             */
            static flatten(options) {
                if (options == null) {
                    return false;
                }
                if (typeof options === 'boolean') {
                    return options;
                }
                return !!options.capture;
            }

            /**
             * https://dom.spec.whatwg.org/#event-flatten-more
             * @param options
             * @returns {{once: boolean, capture: boolean, passive: boolean}}
             */
            static flattenMore(options) {
                const capture = this.flatten(options);
                let once = false;
                let passive = false;
                if (options != null) {
                    once = !!options.once;
                    passive = !!options.passive;
                }
                return {
                    capture,
                    once,
                    passive
                };
            }

            /**
             * @see {@link https://dom.spec.whatwg.org/#add-an-event-listener}
             * @param {EventTarget} eventTarget
             * @param {object} listener
             */
            static addAnEventListener(eventTarget, listener) {
                // TODO: If eventTarget is a ServiceWorkerGlobalScope object, its service worker’s script resource’s has ever been evaluated flag is set, and listener’s type matches the type attribute value of any of the service worker events, then report a warning to the console that this might not give the expected results. [SERVICE-WORKERS]
                if (listener.callback == null) {
                    return;
                }
                for (let i = 0; i < eventTarget.eventListenerList.length; ++i) {
                    const targetListener = eventTarget.eventListenerList[i];
                    if (targetListener.type === listener.type && targetListener.callback === listener.callback && targetListener.capture === listener.capture) {
                        return;
                    }
                }
                eventTarget.eventListenerList.push(listener);
            }

            /**
             * @see {@link https://dom.spec.whatwg.org/#remove-an-event-listener}
             * @param {EventTarget} eventTarget
             * @param {object} listener
             */
            static removeAnEventListener(eventTarget, listener) {
                // TODO: If eventTarget is a ServiceWorkerGlobalScope object and its service worker’s set of event types to handle contains type, then report a warning to the console that this might not give the expected results. [SERVICE-WORKERS]
                listener.removed = true;
                for (let i = 0; i < eventTarget.eventListenerList.length; ++i) {
                    const targetListener = eventTarget.eventListenerList[i];
                    if (targetListener === listener) {
                        eventTarget.eventListenerList.split(i, 1);
                        break;
                    }
                }
            }

            /**
             * @see {@link https://dom.spec.whatwg.org/#concept-event-dispatch}
             * @param {Event} event
             * @param {EventTarget} target The EventTarget onto which the event is dispatched.
             * @param {object} flags Contains legacyTargetOverrideFlag and legacyOutputDidListenersThrowFlag flags. The second one is pass-by-reference.
             * @returns {boolean} Returns the canceled flag after executing the listeners.
             */
            static dispatch(event, target, flags = {}) {
                // 1. Set event’s dispatch flag.
                event.dispatch = true;
                // 2. Let targetOverride be target, if legacy target override flag is not given, and target’s associated Document otherwise. [HTML]
                let targetOverride = target;
                if (flags.legacyTargetOverrideFlag && platform.is('Window') && platform.hasOwnImplementation(platform.global) && platform.getOwnImplementation(platform.global) === target && target.associatedDocument != null) {
                    // If HTML is implemented, and the target is the window, targetOverride will be the window.document.
                    targetOverride = targetOverride.associatedDocument;
                }
                // 3. Let activationTarget be null.
                let activationTarget = null;
                // 4. Let relatedTarget be the result of retargeting event’s relatedTarget against target.
                const relatedTarget = DOM.ShadowRoot.retarget(event.relatedTarget, target);
                let clearTargets = false;
                // 5. If target is not relatedTarget or target is event’s relatedTarget, then:
                if (target !== relatedTarget || target === event.relatedTarget) {
                    // 5.1. Let touchTargets be a new list.
                    let touchTargets = [];
                    // 5.2. For each touchTarget of event’s touch target list, append the result of retargeting touchTarget against target to touchTargets.
                    for (const touchTarget of event.touchTargetList) {
                        touchTargets.push(DOM.ShadowRoot.retarget(touchTarget, target));
                    }
                    // 5.3. Append to an event path with event, target, targetOverride, relatedTarget, touchTargets, and false.
                    this.appendToAnEventPath(event, target, targetOverride, relatedTarget, touchTargets, false);
                    // 5.4. Let isActivationEvent be true, if event is a MouseEvent object and event’s type attribute is "click", and false otherwise.
                    let isActivationEvent = false;
                    if (typeof DOM.MouseEvent === 'function' && event instanceof DOM.MouseEvent && event.type === 'click') {
                        isActivationEvent = true;
                    }
                    // 5.5. If isActivationEvent is true and target has activation behavior, then set activationTarget to target.
                    if (isActivationEvent && typeof target.activationBehavior === 'function') {
                        activationTarget = target;
                    }
                    // 5.6. Let slottable be target, if target is a slottable and is assigned, and null otherwise.
                    let slottable = target instanceof DOM.Slottable && target.assigned ? target : null;
                    // 5.7. Let slot-in-closed-tree be false.
                    let slotInClosedTree = false;
                    // 5.8. Let parent be the result of invoking target’s get the parent with event.
                    let parent = target.getTheParent(event);
                    // 5.9. While parent is non-null:
                    while (parent != null) {
                        // 5.9.1. If slottable is non-null:
                        if (slottable != null) {
                            // 5.9.1.1. Assert: parent is a slot.
                            if (!DOM.Node.isSlot(parent)) {
                                const error = new Error('"parent" is not a "slot" in 5.9.1.1 of EventTarget.prototype.dispatch implementation');
                                error.code = 'ERR_DOM_ASSERT';
                                error.className = 'Event';
                                error.methodName = 'dispatch';
                                error.isImplementation = true;
                                error.step = '5.9.1.1';
                                error.spec = 'https://dom.spec.whatwg.org/#concept-event-dispatch';
                                throw error;
                            }
                            // 5.9.1.2. Set slottable to null.
                            slottable = null;
                            // 5.9.1.3. If parent’s root is a shadow root whose mode is "closed", then set slot-in-closed-tree to true.
                            const root = parent.root;
                            if (root instanceof DOM.ShadowRoot && root.mode === 'closed') {
                                slotInClosedTree = true;
                            }
                        }
                        // 5.9.2. If parent is a slottable and is assigned, then set slottable to parent.
                        if (parent instanceof DOM.Slottable && parent.assigned) {
                            slottable = parent;
                        }
                        // 5.9.3. Let relatedTarget be the result of retargeting event’s relatedTarget against parent.
                        const relatedTarget = DOM.ShadowRoot.retarget(event.relatedTarget, parent);
                        // 5.9.4. Let touchTargets be a new list.
                        touchTargets = [];
                        // 5.9.5. For each touchTarget of event’s touch target list, append the result of retargeting touchTarget against parent to touchTargets.
                        for (const touchTarget of event.touchTargetList) {
                            touchTargets.push(DOM.ShadowRoot.retarget(touchTarget, parent));
                        }
                        // 5.9.6. If parent is a Window object, or parent is a node and target’s root is a shadow-including inclusive ancestor of parent, then:
                        if (parent === platform.getOwnImplementation(platform.global) && platform.is('Window') || parent instanceof DOM.Node && target.root.isShadowIncludingInclusiveAncestorOf(parent)) {
                            // 5.9.6.1. If isActivationEvent is true, event’s bubbles attribute is true, activationTarget is null, and parent has activation behavior, then set activationTarget to parent.
                            if (isActivationEvent && event.bubbles && activationTarget != null && typeof parent.activationBehavior === 'function') {
                                activationTarget = parent;
                            }
                            // 5.9.6.2. Append to an event path with event, parent, null, relatedTarget, touchTargets, and slot-in-closed-tree.
                            this.appendToAnEventPath(event, parent, null, relatedTarget, touchTargets, slotInClosedTree);
                        } else if (parent === relatedTarget) {
                            // 5.9.7. Otherwise, if parent is relatedTarget, then set parent to null.
                            parent = null;
                        } else {
                            // 5.9.8. Otherwise, set target to parent and then:
                            target = parent;
                            // 5.9.8.1. If isActivationEvent is true, activationTarget is null, and target has activation behavior, then set activationTarget to target.
                            if (isActivationEvent && activationTarget == null && typeof target.activationBehavior === 'function') {
                                activationTarget = target;
                            }
                            // 5.9.8.2. Append to an event path with event, parent, target, relatedTarget, touchTargets, and slot-in-closed-tree.
                            this.appendToAnEventPath(event, parent, target, relatedTarget, touchTargets, slotInClosedTree);
                        }
                        // 5.9.9. If parent is non-null, then set parent to the result of invoking parent’s get the parent with event.
                        if (parent != null) {
                            parent = parent.getTheParent(event);
                        }
                        // 5.9.10. Set slot-in-closed-tree to false.
                        slotInClosedTree = false;
                    }
                    // 5.10. Let clearTargetsStruct be the last struct in event’s path whose shadow-adjusted target is non-null.
                    let clearTargetsStruct = null;
                    for (let i = event.path.length - 1; i >= 0; --i) {
                        if (event.path[i].shadowAdjustedTarget != null) {
                            clearTargetsStruct = event.path[i];
                            event.target = clearTargetsStruct.shadowAdjustedTarget;
                            break;
                        }
                    }
                    // 5.11. Let clearTargets be true if clearTargetsStruct’s shadow-adjusted target, clearTargetsStruct’s relatedTarget, or an EventTarget object in clearTargetsStruct’s touch target list is a node and its root is a shadow root, and false otherwise.
                    if (clearTargetsStruct != null) {
                        (() => {
                            if (clearTargetsStruct.shadowAdjustedTarget instanceof DOM.Node) {
                                const root = clearTargetsStruct.shadowAdjustedTarget.root;
                                if (root instanceof DOM.ShadowRoot) {
                                    clearTargets = true;
                                    return;
                                }
                            }
                            if (clearTargetsStruct.relatedTarget instanceof DOM.Node) {
                                const root = clearTargetsStruct.relatedTarget.root;
                                if (root instanceof DOM.ShadowRoot) {
                                    clearTargets = true;
                                    return;
                                }
                            }
                            for (const touchTarget of clearTargetsStruct.touchTargetList) {
                                if (touchTarget instanceof DOM.EventTarget && touchTarget instanceof DOM.Node) {
                                    const root = touchTarget.root;
                                    if (root instanceof DOM.ShadowRoot) {
                                        clearTargets = true;
                                        return;
                                    }
                                }
                            }
                        })();
                    }
                    // 5.12. If activationTarget is non-null and activationTarget has legacy-pre-activation behavior, then run activationTarget’s legacy-pre-activation behavior.
                    if (activationTarget != null && typeof activationTarget.legacyPreActivationBehavior === 'function') {
                        activationTarget.legacyPreActivationBehavior(event);
                    }
                    // 5.13. For each struct in event’s path, in reverse order:
                    for (let i = event.path.length - 1; i >= 0; --i) {
                        const struct = event.path[i];
                        if (struct.shadowAdjustedTarget != null) {
                            // 5.13.1. If struct’s shadow-adjusted target is non-null, then set event’s eventPhase attribute to AT_TARGET.
                            event.eventPhase = DOM.Event.phase.AT_TARGET;
                        } else {
                            // 5.13.2. Otherwise, set event’s eventPhase attribute to CAPTURING_PHASE.
                            event.eventPhase = DOM.Event.phase.CAPTURING_PHASE;
                        }
                        // 5.13.3. Invoke with struct, event, "capturing", and legacyOutputDidListenersThrowFlag if given.
                        // WARNING: "legacyOutputDidListenersThrowFlag" is to be modified, but JavaScript has no pass-by-reference arguments!
                        // NOTE: "Invoke" makes a concept for the "preceding" struct and require "struct" parameter, this results in the need to find the index of the struct within event.path, however, we already know the index here, so it is pointless search.
                        this.invoke(event, i, 'capturing', flags);
                    }
                    // 5.14. For each struct in event’s path:
                    for (let i = 0; i < event.path.length; ++i) {
                        const struct = event.path[i];
                        if (struct.shadowAdjustedTarget) {
                            // 5.14.1. If struct’s shadow-adjusted target is non-null, then set event’s eventPhase attribute to AT_TARGET.
                            event.eventPhase = DOM.Event.phase.AT_TARGET;
                        } else {
                            // 5.14.2. Otherwise:
                            // 5.14.2.1. If event’s bubbles attribute is false, then continue.
                            if (!event.bubbles) {
                                continue;
                            }
                            // 5.14.2.2. Set event’s eventPhase attribute to BUBBLING_PHASE.
                            event.eventPhase = DOM.Event.phase.BUBBLING_PHASE;
                        }
                        // 5.14.3. Invoke with struct, event, "bubbling", and legacyOutputDidListenersThrowFlag if given.
                        // WARNING: "legacyOutputDidListenersThrowFlag" is to be modified, but JavaScript has no pass-by-reference arguments!
                        // NOTE: "Invoke" makes a concept for the "preceding" struct and require "struct" parameter, this results in the need to find the index of the struct within event.path, however, we already know the index here, so it is pointless search.
                        this.invoke(event, i, 'bubbling', flags);
                    }
                }
                // 6. Set event’s eventPhase attribute to NONE.
                event.eventPhase = DOM.Event.phase.NONE;
                // 7. Set event’s currentTarget attribute to null.
                event.currentTarget = null;
                // 8. Set event’s path to the empty list.
                event.path = [];
                // 9. Unset event’s dispatch flag, stop propagation flag, and stop immediate propagation flag.
                event.dispatch = false;
                event.stopPropagation = false;
                event.stopImmediatePropagation = false;
                // 10. If clearTargets, then:
                if (clearTargets) {
                    // 10.1. Set event’s target to null.
                    event.target = null;
                    // 10.2. Set event’s relatedTarget to null.
                    event.relatedTarget = null;
                    // 10.3. Set event’s touch target list to the empty list.
                    event.touchTargetList = [];
                }
                // 11. If activationTarget is non-null, then:
                if (activationTarget != null) {
                    if (!event.canceled) {
                        // 11.1. If event’s canceled flag is unset, then run activationTarget’s activation behavior with event.
                        if (typeof activationTarget.activationBehavior === 'function') {
                            activationTarget.activationBehavior(event);
                        }
                    } else {
                        // 11.2. Otherwise, if activationTarget has legacy-canceled-activation behavior, then run activationTarget’s legacy-canceled-activation behavior.
                        if (typeof activationTarget.legacyPreActivationBehavior === 'function') {
                            activationTarget.legacyPreActivationBehavior(event);
                        }
                    }
                }
                // 12. Return false if event’s canceled flag is set, and true otherwise.
                return !event.canceled;
            }

            /**
             * @see {@link https://dom.spec.whatwg.org/#concept-event-path-append}
             * @param {Event} event
             * @param {Node} invocationTarget
             * @param {Node} shadowAdjustedTarget
             * @param {Node} relatedTarget
             * @param {Array<Node>}touchTargetList
             * @param {boolean} slotInClosedTree
             */
            static appendToAnEventPath(event, invocationTarget, shadowAdjustedTarget, relatedTarget, touchTargetList, slotInClosedTree) {
                // 1. Let invocationTargetInShadowTree be false.
                let invocationTargetInShadowTree = false;
                // 2. If invocationTarget is a node and its root is a shadow root, then set invocationTargetInShadowTree to true.
                if (invocationTarget instanceof DOM.Node && invocationTarget.root instanceof DOM.ShadowRoot) {
                    invocationTargetInShadowTree = true;
                }
                // 3. Let root-of-closed-tree be false.
                let rootOfClosedTree = false;
                // 4. If invocationTarget is a shadow root whose mode is "closed", then set root-of-closed-tree to true.
                if (invocationTarget instanceof DOM.ShadowRoot && invocationTarget.mode === 'closed') {
                    rootOfClosedTree = true;
                }
                // 5. Append a new struct to event’s path whose invocation target is invocationTarget, invocation-target-in-shadow-tree is invocationTargetInShadowTree, shadow-adjusted target is shadowAdjustedTarget, relatedTarget is relatedTarget, touch target list is touchTargets, root-of-closed-tree is root-of-closed-tree, and slot-in-closed-tree is slot-in-closed-tree.
                event.path.push({
                    invocationTarget,
                    invocationTargetInShadowTree,
                    shadowAdjustedTarget,
                    relatedTarget,
                    touchTargetList: [...touchTargetList],
                    rootOfClosedTree,
                    slotInClosedTree
                });
            }

            /**
             * @see {@link https://dom.spec.whatwg.org/#concept-event-listener-invoke}
             * @param {Event} event
             * @param {number} index
             * @param {number} phase
             * @param {object} flags
             */
            static invoke(event, index, phase, flags) {
                // Originally "struct" must be an argument. However, step 1 requires "preceding" struct. The caller knows the "struct" index, but we would not
                // know it, if we follow the standard. Therefore, we should take additional search to find the "struct" within the "event.path" before taking
                // step 1. By passing the "pathIndex" instead, we can optimize this search.
                const struct = event.path[index];
                // 1. Set event’s target to the shadow-adjusted target of the last struct in event’s path, that is either struct or preceding struct, whose shadow-adjusted target is non-null.
                for (let i = index - 1; i >= 0; --i) {
                    if (event.path[i].shadowAdjustedTarget != null) {
                        event.target = event.path[i].shadowAdjustedTarget;
                        break;
                    }
                }
                // 2. Set event’s relatedTarget to struct’s relatedTarget.
                event.relatedTarget = struct.relatedTarget;
                // 3. Set event’s touch target list to struct’s touch target list.
                event.touchTargetList = struct.touchTargetList;
                // 4. If event’s stop propagation flag is set, then return.
                if (event.stopPropagation) {
                    return;
                }
                // 5. Initialize event’s currentTarget attribute to struct’s invocation target.
                event.currentTarget = struct.invocationTarget;
                // 6. Let listeners be a clone of event’s currentTarget attribute value’s event listener list.
                const listeners = [...event.currentTarget.eventListenerList];
                // 7. Let invocationTargetInShadowTree be struct’s invocation-target-in-shadow-tree.
                const invocationTargetInShadowTree = struct.invocationTargetInShadowTree;
                // 8. Let found be the result of running inner invoke with event, listeners, phase, and legacyOutputDidListenersThrowFlag if given.
                const found = this.innerInvoke(event, listeners, phase, invocationTargetInShadowTree, flags);
                // 9. If found is false and event’s isTrusted attribute is true, then:
                if (!found && event.isTrusted && typeof this.invokeWebkitOverrides === 'function') {
                    // This SHOULD NOT be part of the standard. Webkit is not a standard engine, we should not care of its overrides.
                    this.invokeWebkitOverrides(event, index, phase, flags);
                }
            }

            /**
             * @see {@link https://dom.spec.whatwg.org/#concept-event-listener-inner-invoke}
             * @param {Event} event
             * @param {Array<object>} listeners
             * @param {string} phase
             * @param {boolean} invocationTargetInShadowTree
             * @param {object} flags
             * @returns {boolean}
             */
            static innerInvoke(event, listeners, phase, invocationTargetInShadowTree, flags) {
                // 1. Let found be false.
                let found = false;
                // 2. For each listener in listeners, whose removed is false:
                for (const listener of listeners) {
                    if (listener.removed) {
                        continue;
                    }
                    // 2.1. If event’s type attribute value is not listener’s type, then continue.
                    if (listener.type !== event.type) {
                        continue;
                    }
                    // 2.2. Set found to true.
                    found = true;
                    // 2.3. If phase is "capturing" and listener’s capture is false, then continue.
                    if (phase === 'capturing' && !listener.capture) {
                        continue;
                    }
                    // 2.4. If phase is "bubbling" and listener’s capture is true, then continue.
                    if (phase === 'bubbling' && listener.capture) {
                        continue;
                    }
                    // 2.5. If listener’s once is true, then remove listener from event’s currentTarget attribute value’s event listener list.
                    if (listener.once) {
                        const index = event.currentTarget.eventListenerList.indexOf(listener);
                        if (index >= 0) {
                            event.currentTarget.eventListenerList.splice(index, 1);
                        }
                    }
                    // 2.6. Let global be listener callback’s associated Realm’s global object.
                    const global = platform.global;
                    // 2.7. Let currentEvent be undefined.
                    let currentEvent;
                    // 2.8. If global is a Window object, then:
                    if (platform.is('Window') && platform.hasOwnImplementation(platform.global)) {
                        const window = platform.getImplementation(platform.global);
                        // 2.8.1. Set currentEvent to global’s current event.
                        currentEvent = window.currentEvent;
                        // 2.8.2. If struct’s invocation-target-in-shadow-tree is false, then set global’s current event to event.
                        if (!invocationTargetInShadowTree) {
                            window.currentEvent = event;
                        }
                    }
                    // 2.9 If listener’s passive is true, then set event’s in passive listener flag.
                    if (listener.passive) {
                        event.inPassiveListener = true;
                    }
                    // 2.10. Call a user object’s operation with listener’s callback, "handleEvent", « event », and event’s currentTarget attribute value. If this throws an exception, then:
                    try {
                        this.callUserObjectOperation(listener.callback, 'handleEvent', [platform.getInterface(event)], platform.getInterface(event.currentTarget));
                    } catch (e) {
                        // 2.10.1. Report the exception.
                        if (typeof DOM.reportAnException === 'function') {
                            // Note: there is no such concept in DOM specification "report an exception", so we make it optional.
                            DOM.reportAnException(e);
                        }
                        // 2.10.2. Set legacyOutputDidListenersThrowFlag if given.
                        if (flags.legacyOutputDidListenersThrowFlag != null) {
                            // The legacyOutputDidListenersThrowFlag is only used by Indexed Database API.
                            flags.legacyOutputDidListenersThrowFlag = true;
                        }
                    }
                    // 2.11. Unset event’s in passive listener flag.
                    event.inPassiveListener = false;
                    // 2.12. If global is a Window object, then set global’s current event to currentEvent.
                    if (platform.is('Window') && platform.hasOwnImplementation(platform.global)) {
                        const window = platform.getImplementation(global);
                        window.currentEvent = currentEvent;
                    }
                    // 2.13. If event’s stop immediate propagation flag is set, then return found.
                    if (event.stopImmediatePropagation) {
                        return found;
                    }
                }
                // 3. Return found.
                return found;
            }

            /**
             * @see {@link https://heycam.github.io/webidl/#call-a-user-objects-operation}
             * @param {function|object} value
             * @param {string} opName
             * @param {Array} args
             * @param {object} thisArg
             */
            static callUserObjectOperation(value, opName, args, thisArg) {
                // 9. Let X be O.
                let x = value;
                // 10. If ! IsCallable(O) is false, then:
                if (typeof value === 'object') {
                    // 10.1. Let getResult be Get(O, opName).
                    const getResult = value[opName];
                    // 10.4. If ! IsCallable(X) is false, then set completion to a new Completion{[[Type]]: throw, [[Value]]: a newly created TypeError object, [[Target]]: empty}, and jump to the step labeled return.
                    if (typeof getResult !== 'function') {
                        // The implementations seems to ignore this while it propagates a errors thrown in the listener.
                        return;
                    }
                    // 10.3. Set X to getResult.[[Value]].
                    x = getResult;
                    // 10.5. Set thisArg to O (overriding the provided value).
                    thisArg = value;
                }
                // 12. Let callResult be Call(X, thisArg, esArgs).
                return platform.apply(x, thisArg, args);
            }

            static fireAnEvent(name, target, eventConstructor = null, idl = {}, flags = {}) {
                if (eventConstructor == null) {
                    eventConstructor = DOM.Event;
                }
                const event = DOM.Event.createAnEvent(eventConstructor);
                event.type = name;
                Object.assign(event, idl);
                return this.dispatch(event, target, flags);
            }
        };

        const EventTarget = platform.function(function () {
            const implementation = new DOM.EventTarget();
            platform.setImplementation(this, implementation);
        }, {
            name: 'EventTarget',
            before: validateClassInvocation
        });

        const method = platform.methodFactory({
            before: [
                validateNativeInvocation,
                platform.unwrapThisInterceptor
            ],
            entry: {
                Interface: EventTarget,
                Implementation: DOM.EventTarget,
                class: 'EventTarget'
            },
            forbidNew: true
        });

        EventTarget.prototype = Object.create(platform.primordials['Object.prototype'], {
            constructor: {
                configurable: true,
                writable: true,
                value: EventTarget
            },
            addEventListener: {
                configurable: true,
                enumerable: true,
                writable: true,
                value: method(function (type, callback, options = Object.create(null)) {
                    if (arguments.length < 2) {
                        throw new platform.primordials.TypeError(`Failed to execute 'addEventListener' on 'EventTarget': 2 arguments required, but only ${arguments.length} present.`);
                    }
                    if (callback != null && typeof callback !== 'object' && typeof callback !== 'function') {
                        throw new platform.primordials.TypeError(`Failed to execute 'addEventListener' on 'EventTarget': The callback provided as parameter 2 is not an object.`);
                    }
                    const listener = DOM.EventTarget.flattenMore(options);
                    listener.type = '' + type;
                    listener.callback = callback;
                    DOM.EventTarget.addAnEventListener(this, listener);
                }, {
                    name: 'addEventListener'
                })
            },
            removeEventListener: {
                configurable: true,
                enumerable: true,
                writable: true,
                value: method(function (type, callback, options = Object.create(null)) {
                    if (arguments.length < 2) {
                        throw new platform.primordials.TypeError(`Failed to execute 'removeEventListener' on 'EventTarget': 2 arguments required, but only ${arguments.length} present.`);
                    }
                    if (callback != null && typeof callback !== 'object' && typeof callback !== 'function') {
                        throw new platform.primordials.TypeError(`Failed to execute 'removeEventListener' on 'EventTarget': The callback provided as parameter 2 is not an object.`);
                    }
                    const capture = DOM.EventTarget.flatten(options);
                    type = '' + type;
                    for (let i = 0; i < this.eventListenerList.length; ++i) {
                        const listener = this.eventListenerList[i];
                        if (listener.type === type && listener.callback === callback && listener.capture === capture) {
                            // Optimized remove an event listener: https://dom.spec.whatwg.org/#ref-for-remove-an-event-listener
                            listener.removed = true;
                            this.eventListenerList.splice(i, 1);
                            break;
                        }
                    }
                }, {
                    name: 'removeEventListener'
                })
            },
            dispatchEvent: {
                configurable: true,
                enumerable: true,
                writable: true,
                value: method(function (event) {
                    if (arguments.length < 1) {
                        throw new platform.primordials.TypeError(`Failed to execute 'dispatchEvent' on 'EventTarget': 1 argument required, but only ${arguments.length} present.`);
                    }
                    if (!(event instanceof DOM.Event)) {
                        throw new platform.primordials.TypeError(`Failed to execute 'dispatchEvent' on 'EventTarget': parameter 1 is not of type 'Event'.`);
                    }
                    if (event.dispatch) {
                        throw DOM.DOMException.create(`Failed to execute 'dispatchEvent' on 'EventTarget': The event is already being dispatched.`, 'INVALID_STATE_ERR');
                    }
                    if (!event.initialized) {
                        throw DOM.DOMException.create(`Failed to execute 'dispatchEvent' on 'EventTarget': The event is not initialized.`, 'INVALID_STATE_ERR');
                    }
                    event.isTrusted = false;
                    return DOM.EventTarget.dispatch(event, this);
                }, {
                    name: 'dispatchEvent',
                    before: platform.unwrapArgumentsInterceptorFactory(0)
                })
            },
            [platform.primordials['Symbol.toStringTag']]: {
                configurable: true,
                value: 'EventTarget'
            }
        });

        platform.setImplementation(EventTarget, DOM.EventTarget);
        platform.setImplementation(EventTarget.prototype, DOM.EventTarget.prototype);

        if (platform.is('Window') || platform.is('Worker') || platform.is('AudioWorklet')) {
            Object.defineProperty(platform.global, 'EventTarget', {
                configurable: true,
                writable: true,
                value: EventTarget
            });
        }

        agent[symbols.once] = {
            interface: EventTarget,
            implementation: DOM.EventTarget
        };
    };
})();
