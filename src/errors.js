(function () {
    'use strict';

    exports.validateClassInvocation = function (entry, platform) {
        if (entry.newTarget == null) {
            throw new platform.primordials.TypeError(`Failed to construct '${entry.name}': Please use the 'new' operator, this DOM object constructor cannot be called as a function.`);
        }
    };

    exports.validateNativeInvocation = function (entry, platform) {
        if (!(entry.this instanceof entry.Interface) || !(platform.getImplementation(entry.this) instanceof entry.Implementation)) {
            throw new platform.primordials.TypeError('Illegal invocation');
        }
    };

    exports.disallowConstructor = function () {
        throw new TypeError('Illegal constructor');
    };
})();
