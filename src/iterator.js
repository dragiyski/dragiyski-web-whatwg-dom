(function () {
    'use strict';

    const JavaScript = require('@dragiyski/javascript');

    const symbols = {
        once: Symbol('Iterator')
    };

    /**
     * @param {Platform} platform
     */
    module.exports = function (platform) {
        const agent = platform.getImplementation(platform);
        if (symbols.once in agent) {
            return;
        }

        const { DOM } = agent;

        DOM.Iterator = class Iterator {
        };

        DOM.ArrayIterator = class ArrayIterator {
            constructor(array, transform) {
                if (typeof transform !== 'function') {
                    if (transform == null) {
                        transform = DOM.ArrayIterator.identity;
                    } else {
                        throw new TypeError(`Expected function for arguments[1], got ${JavaScript.getType(transform)}`);
                    }
                }
                this.array = array;
                this.transform = transform;
                this.index = 0;
            }

            next() {
                const done = Boolean(this.index >= this.array.length);
                let value;
                if (!done) {
                    const index = this.index++;
                    value = this.transform(this.array[index], index);
                }
                return Object.create(platform.primordials['Object.prototype'], {
                    value: {
                        configurable: true,
                        enumerable: true,
                        writable: true,
                        value
                    },
                    done: {
                        configurable: true,
                        enumerable: true,
                        writable: true,
                        value: done
                    }
                });
            }

            static identity(value) {
                return value;
            }

            static create(array, transform) {
                const iterator = new this(array, transform);
                const object = Object.create(platform.getInterface(this));
                platform.setImplementation(object, iterator);
                return iterator;
            }
        };

        DOM.Iterator.interfaceIterator = platform.function(function () {
            return this;
        }, {
            name: '[Symbol.iterator]',
            forbidNew: true
        });

        const Iterator = Object.create(platform.primordials['Object.prototype'], {
            [platform.primordials['Symbol.iterator']]: {
                configurable: true,
                writable: true,
                value: DOM.Iterator.interfaceIterator
            }
        });

        platform.setImplementation(Iterator, DOM.Iterator);

        const ArrayIterator = Object.create(Iterator, {
            [Symbol.toStringTag]: {
                configurable: true,
                value: 'Array Iterator'
            },
            next: {
                configurable: true,
                writable: true,
                value: platform.function(function () {
                    return this.next();
                }, {
                    name: 'next',
                    entry: {
                        class: 'Array Iterator'
                    },
                    before: [
                        validateReceiver,
                        platform.unwrapThisInterceptor
                    ],
                    forbidNew: true
                })
            }
        });

        platform.setImplementation(ArrayIterator, DOM.ArrayIterator);

        function validateReceiver(entry, platform) {
            if (!platform.hasImplementation(entry.this)) {
                throw new platform.primordials.TypeError(`Method Array Iterator.prototype.next called on incompatible receiver ${Object.prototype.toString.call(entry.this)}`);
            }
        }
    };
})();
