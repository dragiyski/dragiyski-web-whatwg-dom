(function () {
    'use strict';

    /**
     * @param {Platform} platform The platform implementation. This affects the performance of the module. Therefore, strong dependency (in package.json)
     * is not added. The users should provide either @dragiyski/web-platform (faster, but potentially insecure) or @dragiyski/web-platform-secure (hardened,
     * but requires compilation of C++ natives accessing V8 API), or implement their own platform with the documentation below.
     */
    module.exports = function (platform) {
        const agent = platform.getImplementation(platform);
        agent.DOM = Object.create(null);
        if (!platform.hasOwnImplementation(platform.global)) {
            platform.setImplementation(platform.global, Object.create(null));
        }
        require('./src/iterator')(platform);
        require('./src/spec/DOMException')(platform);
        require('./src/spec/NodeList')(platform);
        require('./src/spec/Event')(platform);
        require('./src/spec/EventTarget')(platform);
        require('./src/spec/AbortController')(platform);
        require('./src/spec/AbortSignal')(platform);
        require('./src/spec/Node')(platform);
        require('./src/spec/DocumentFragment')(platform);
        require('./src/spec/ShadowRoot')(platform);
        require('./src/spec/Slottable')(platform);
    };

    /**
     * @class Platform
     */

    /**
     * Sets the implementation object for an interface object.
     *
     * @function setImplementation
     * @memberOf Platform
     * @instance
     * @param {object} interface An interface object that won't be modified. No properties are added, removed or changed. Prototype won't be affected.
     * @param {object} implementation An implementation object. It might receive a new symbol property.
     */

    /**
     * @function hasOwnImplementation
     * @memberOf Platform
     * @instance
     * @param {object} interface
     * @return {boolean}
     */

    /**
     * @function hasImplementation
     * @memberOf Platform
     * @instance
     * @param {object} interface
     */

    /**
     * @function getOwnImplementation
     * @memberOf Platform
     * @instance
     * @param {object} interface
     */

    /**
     * @function getImplementation
     * @memberOf Platform
     * @instance
     * @param {object} interface
     */

    /**
     * @function hasOwnInterface
     * @memberOf Platform
     * @instance
     * @param {object} implementation
     */

    /**
     * @function hasInterface
     * @memberOf Platform
     * @instance
     * @param {object} implementation
     */

    /**
     * @function getOwnInterface
     * @memberOf Platform
     * @instance
     * @param {object} implementation
     */

    /**
     * @function getInterface
     * @memberOf Platform
     * @instance
     * @param {object} implementation
     */

    /**
     * @typedef {function} PlatformFunctionCallbackNoReturn
     * @param {object} entry
     * @param {object} entry.this
     * @param {IArguments} entry.arguments
     * @param {function|undefined} entry.newTarget
     * @param {string|undefined} entry.name
     * @param {Platform} platform
     */

    /**
     * @typedef {function} PlatformFunctionCallbackReturn
     * @param {*} value
     * @param {object} entry
     * @param {object} entry.this
     * @param {IArguments} entry.arguments
     * @param {function|undefined} entry.newTarget
     * @param {string|undefined} entry.name
     * @param {Platform} platform
     * @return {*} value
     */

    /**
     * @typedef {function} PlatformFunctionCallbackException
     * @param {Error} exception
     * @param {object} entry
     * @param {object} entry.this
     * @param {IArguments} entry.arguments
     * @param {function|undefined} entry.newTarget
     * @param {string|undefined} entry.name
     * @param {Platform} platform
     * @return {Error} exception
     */

    /**
     * @typedef {object} PlatformFunctionOptions
     * @property {PlatformFunctionCallbackNoReturn|Iterable<PlatformFunctionCallbackNoReturn>} before
     * @property {PlatformFunctionCallbackReturn|Iterable<PlatformFunctionCallbackReturn>} after
     * @property {PlatformFunctionCallbackException|Iterable<PlatformFunctionCallbackException>} onCatch
     * @property {PlatformFunctionCallbackNoReturn|Iterable<PlatformFunctionCallbackNoReturn>} onFinally
     * @property {string} name
     * @property {object} entry
     * @property {boolean} forbidNew
     */

    /**
     * @function function
     * @memberOf Platform
     * @instance
     * @param {function} implementation
     * @param {PlatformFunctionOptions} options
     * @return {function}
     */

    /**
     * @function methodFactory
     * @memberOf Platform
     * @instance
     * @param {PlatformFunctionOptions} options
     * @return {function}
     */

    /**
     * @function is
     * @memberOf Platform
     * @instance
     * @param {string} type
     * @return {boolean}
     */

    /**
     * @var {object} global
     * @memberOf Platform
     * @instance
     */

    /**
     * @var {object} primordials
     * @memberOf Platform
     * @instance
     */

    /**
     * @var {PlatformFunctionCallbackNoReturn} unwrapThisInterceptor
     * @memberOf Platform
     * @instance
     */

    /**
     * @var {PlatformFunctionCallbackNoReturn} unwrapOwnThisInterceptor
     * @memberOf Platform
     * @instance
     */

    /**
     * @var {PlatformFunctionCallbackNoReturn} unwrapAllArgumentsInterceptor
     * @memberOf Platform
     * @instance
     */

    /**
     * @var {PlatformFunctionCallbackNoReturn} unwrapOwnAllArgumentsInterceptor
     * @memberOf Platform
     * @instance
     */

    /**
     * @function unwrapArgumentsInterceptorFactory
     * @memberOf Platform
     * @instance
     * @param {...number}
     * @return {PlatformFunctionCallbackNoReturn}
     */

    /**
     * @function unwrapOwnArgumentsInterceptorFactory
     * @memberOf Platform
     * @instance
     * @param {...number}
     * @return {PlatformFunctionCallbackNoReturn}
     */

    /**
     * @var {PlatformFunctionCallbackReturn} wrapReturnValueInterceptor
     * @memberOf Platform
     * @instance
     */

    /**
     * @var {PlatformFunctionCallbackReturn} wrapOwnReturnValueInterceptor
     * @memberOf Platform
     * @instance
     */

    /**
     * @function call
     * @memberOf Platform
     * @instance
     * @param {function} callee
     * @param {object} thisArg
     * @param {...*} args
     */

    /**
     * @function bind
     * @memberOf Platform
     * @instance
     * @param {function} callee
     * @param {object} thisArg
     * @param {...*} args
     */

    /**
     * @function apply
     * @memberOf Platform
     * @instance
     * @param {function} callee
     * @param {object} thisArg
     * @param {ArrayLike} args
     */
})();
